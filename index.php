<!DOCTYPE html>
<html lang="fr">

  <head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Shifumi</title>
    <link rel="stylesheet" href="assets/css/style.css">
  </head>

  <body>

    <div class="container">
      <div class="stats">
        <label>Victoire de HAL: <b><?php echo get_cookies("hal_score", "0") ?></b></label>
        <label>En jeux depuis: <b><?php echo get_cookies("time_started", "00h 00m") ?></b></label>
      </div>
      <header>
        <h1>
          <span>Rock</span>
          <span>Paper</span>
          <span>Scissors</span>
        </h1>
        <div class="score-container">
          <h4>Score</h4>
          <p id="score"><?php echo get_cookies("user_score", "0") ?></p>
        </div>
      </header>

      <main>
        <button class="btn-circle btn-paper" onclick="window.location.href='./result?play=paper'">
          <span class="wrapper">  
            <img src="./assets/img/paper.png" alt="paper icon" />
          </span>
        </button>
        <button class="btn-circle btn-scissors" onclick="window.location.href='./result?play=scissors'">
          <span class="wrapper">  
            <img src="./assets/img/scissors.png" alt="scissors icon" />
          </span>
        </button>
        <button class="btn-circle btn-rock" onclick="window.location.href='./result?play=rock'">
          <span class="wrapper">  
            <img src="./assets/img/rock.png" alt="rock icon" />
          </span>
        </button>
      </main>

      <button class="btn btn-rules">Rules</button>
    </div>


  </body>

  <script src="assets/js/script.js"></script>

  <?php 
    function get_cookies($key, $default){
      if(isset($_COOKIE[$key])){
        return $_COOKIE[$key];
      }
      return $default;
    }
  ?>

</html>