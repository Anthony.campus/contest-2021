<!DOCTYPE html>
<html lang="fr">

    <head>
        <meta charset="UTF-8">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <title>Shifumi</title>
        <link rel="stylesheet" href="assets/css/style.css">
    </head>

    <body>
        <?php
        date_default_timezone_set("CET");
        $ai_answer = get_ai_answer();

        if(get_cookies("time_started", null) == null){
            setcookie("time_started", date('H:i'));
        }
        
        function get_user_answer(){
            if(isset($_GET['play'])){
                return $_GET['play'];
            }else{
                header('Location: ./');
            }
        }

        function get_ai_answer(){
            $ai = rand(1, 3);
            if ($ai == 1){
                $ai = "rock";
            }else if ($ai == 2){
                $ai = "paper";
            }else if ($ai == 3){
                $ai = "scissors";
            }
            return $ai;
        }

        function search_winner($ai){
            $user = get_user_answer();
            $result = null;

            if ($user == $ai){
                $result = "Draw";
            }else if (($user == "rock" && $ai == "paper")  ($user == "paper" && $ai == "scissors")  ($user == "scissors" && $ai == "rock")){
                $result = "Lose";
                add_score("hal_score");
            }else if (($user == "paper" && $ai == "rock")  ($user == "scissors" && $ai == "paper")  ($user == "rock" && $ai == "scissors")){
                $result = "Win";

                add_score("user_score");
            }

            return $result;
        }

        function add_score($key){
            try {
                $value = get_cookies($key, 0);
                $value = (int)$value;
                $value += 1;
                setcookie($key, $value);
                return $value;
            } catch (Exception $e) {
               return 0;
            }
        }
        ?>
        
        <div class="container">
            <header>
                <h1>
                <span>Rock</span>
                <span>Paper</span>
                <span>Scissors</span>
                </h1>
                <div class="score-container">
                <h4>Score</h4>
                <p id="score"><?php echo get_cookies("user_score", "0") ?></p>
                </div>
            </header>
            <button class="btn btn-rules">Rules</button>
        </div>

        <div class="selection">
                <div>
                    <h2>You picked</h2>
                    <button <?php echo('class="btn-circle btn-'.get_user_answer().'"') ?> onclick="window.location.href='./result?play=paper'">
                        <span class="wrapper">
                            <img <?php echo('src="./assets/img/'.get_user_answer().'.png"') ?> alt="paper icon" />
                        </span>
                    </button>
                </div>

                <div>
                    <p class="big-text">You <span id="win"><?php echo(search_winner($ai_answer)); ?></span></p>
                    <button class="btn btn-play-again" id="reset" onclick="window.location.href='./'">Play again</button>
                </div>

                <div>
                    <h2>HAL picked</h2>
                    <button <?php echo('class="btn-circle btn-'.$ai_answer.'"') ?> onclick="window.location.href='./result?play=scissors'">
                        <span class="wrapper">
                            <img <?php echo('src="./assets/img/'.$ai_answer.'.png"') ?> alt="scissors icon" />
                        </span>
                    </button>
                </div>
            </div>

    </body>

    <script src="assets/js/script.js"></script>

    <?php 

    function get_cookies($key, $default){
      if(isset($_COOKIE[$key])){
        return $_COOKIE[$key];
      }
      return $default;
    }
  ?>
  
</html>